/**
 * TOOLBOX.js
 * by Magnus Fredlundh { magnusfredlundh@gmail.com }
 * 
 */

var tb = {}; // The Toolbox...



/* My anonymous function */
(function(document, window){
  var document = document,
      window = window;

  /* GET_PARENT
   * @param element - The element that we want to find the parent of.
   * @param classname - (Optional) The class the parent should have.
   */

  tb.get_parent = function(params) {
    if( !params || !params.element ) { return false; }

    if(params.classname) {
      var element = params.element;

      while(element.parentNode) {
        element = element.parentNode;
        if(element && element.getAttribute) {
          if(tb.has_class({element: element, classname: params.classname})) { return element; }
        }
      }
    }
    else {
      return params.element.parentNode;
    }

    // We didn't found a match so we return false;
    return false;
  };



  /* HAS_CLASS
   * @param element - The element that we want to check.
   * @param classname - The class we want to check for.
   */

  tb.has_class = function(params) {
    if(!params || !params.element || !params.classname) { return false; }

    // Checking if the browser has iplementet the classList.
    if(params.element.classList) {
      return params.element.classList.contains(params.classname.replace(/\s+/g,''));
    }
    // If it doesn't then we do it old-school!
    else {
      // Spliting the value of the class attribute.
      var classnames = params.element.getAttribute('class').split(' '),
          length = classNames.length,
          classToMatch = params.classname.toLowerCase().replace(/\s+/g,'');

      // Checking if one of them is what we are searching for.
      while(length--) {
        if(classnames[length].toLowerCase() === classToMatch) {
          return true;
        }
      }

      // We return false if we didn't got a match
      return false;

    }
  };

})(document, window);
console.group("Debuging get_parent");
  console.debug("get_parent(no class):", tb.get_parent({element: document.getElementById("foo")}));
  console.debug("get_parent(no class, null element):", tb.get_parent({element: document.getElementById("foob")}));
  console.debug("get_parent(class: bar):", tb.get_parent({element: document.getElementById("foo"), classname: 'bar'}));
  console.debug("get_parent(class: foo):", tb.get_parent({element: document.getElementById("foo"), classname: 'foo'}));
  console.debug("get_parent(class: noo):", tb.get_parent({element: document.getElementById("foo"), classname: 'noo'}));
console.groupEnd();

console.group("Debuging has_class");
  console.debug("has_class:", tb.has_class({element: document.getElementsByTagName('body')[0], classname: 'doo'}));
  console.debug("has_class(null element):", tb.has_class({element: document.getElementsByTagName('bodyy')[0], classname: 'doo'}));
  console.debug("has_class(no element):", tb.has_class({classname: 'doo'}));
console.groupEnd();